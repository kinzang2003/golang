// package main

// import "fmt"

// func main() {
// 	fmt.Println("Hello World")
// }

package main

import (
	"fmt"
	"mygo/mypackage"
)

func main() {
	fmt.Println("Hello World")
	mypackage.Support()
}
