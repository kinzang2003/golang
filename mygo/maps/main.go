package main

func main() {
	colors := map[string]string{
		"red":   "danger",
		"green": "nature",
		"blue":  "sky",
	}
	// delete(colors, "red")
	// fmt.Println(colors)
	printMap(colors)
}

func printMap(c map[string]string) {
	for color, def := range c {
		println("definition of " + color + " is " + def)
	}
}

// var mymap[string] map {"a": "apple", "b": "mango"}
// for val := range mymap{
// 	fmt.Println(val)
// }
