package main

import "fmt"

/* interface -> custom type -> set of abstract methods */
type bot interface {
	greet() string
}

type englishbot struct {
}

type dzongkhabot struct {
}

func main() {
	eb := englishbot{}
	db := dzongkhabot{}

	printGreeting(eb)
	printGreeting(db)
}

func (englishbot) greet() string {
	return "hello"
}
func (dzongkhabot) greet() string {
	return "kuzuzangpo"
}

//	func printGreeting(eb englishBot) {
//		fmt.Println(eb.greet())
//	}
//
//	func printGreeting(db dzongkhabot) {
//		fmt.Println(db.greet())
//	}
func printGreeting(b bot) {
	fmt.Println(b.greet())
}
