package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	resp, err := http.Get("http://www.apple.com")
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	// no error
	fmt.Println(resp)
	byteSlice := make([]byte, 999999)
	resp.Body.Read(byteSlice)
	fmt.Println(string(byteSlice))
}
