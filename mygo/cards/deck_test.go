package main

import "testing"

func TestNewDeck(t *testing.T) {
	d := newDeck()
	if len(d) != 16 {
		t.Errorf("Expected deck length of 20 but got %d", len(d))
	}

	//test2
	if d[0] != "One of Club" {
		t.Errorf("Expected One of Club but found %v", d[0])
	}

	//test3
	if d[len(d)-1] != "Four of Spades" {
		t.Errorf("Expected Four of Spades but found %v", d[0])
	}
}
