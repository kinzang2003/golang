package main

import (
	"fmt"
	"math/rand"
)

type deck []string

func (d deck) sommething() {
	for index, value := range d {
		fmt.Println(index, value)
	}
}

func newDeck() deck {
	cards := deck{}
	suits := []string{"Club", "Heart", "Diamond", "Spades"}
	values := []string{"One", "two", "Three", "Four"}

	for _, s := range suits {
		for _, v := range values {
			cards = append(cards, v+" of "+s)
		}
	}
	return cards
}

// func (d deck) deal(handSize int) (deck, deck) { //func deal(d deal, handSize int) (deck, deck) { when not using reciever
// return d[:handSize], d[handSize:] // both index start with 0
// }

// helper function to saveToFile function

// func (d deck) toString() string {
// 	return strings.Join([]string(d), ",")
// }

// func (d deck) saveToFile(fileName string) error {
// 	return os.WriteFile(fileName, []byte(d.toString()), 0666)
// }

/* HW_1 */
// func newDeckFromFile(fileName string) deck {
// 	byteSlice, err := os.ReadFile(fileName)
// 	if err != nil {
// 		fmt.Println("error:", err)
// 		os.Exit(1)
// 	}
// 	// convert byteSlice to deck
// 	str := strings.Split(string(byteSlice), ",")
// 	return deck(str) // convert str to deck type

// }

/* HW_2 */
func (d deck) shuffle() {
	for i := range d {
		// generate new random index
		newIndex := rand.Intn(len(d) - 1)
		//swap
		d[i], d[newIndex] = d[newIndex], d[i]
	}
}
