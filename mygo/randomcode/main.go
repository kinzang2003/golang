package main

import "fmt"

func main() {
	str, ok := weird()
	if ok {
		fmt.Println("A")
	}
	if str == "Dorji" { // note that there is 3 'if'
		fmt.Println("B")
	}
	if str == "Singye" {
		fmt.Println("C")
	}
}
func weird() (string, bool) {
	return "Dorji", true
}
