package main

import "fmt"

type person struct {
	firstname string
	lastname  string
	address   contactInfo // contactInfo
}

func main() {

	// var Kuenga person
	kuenga := person{
		firstname: "kuenga",
		lastname:  "tshering",
		address: contactInfo{
			email:  "kuenga@gmail.com",
			mobile: 17171718,
		},
		/* if  field name is not mentioned
		contactInfo: contactInfo{
			email:  "kuenga@gmail.com",
			mobile: 17171718,
		}*/
	}

	// Kuenga.firstname = "kuenga"
	// Kuenga.lastname = "tshering"

	// fmt.Println(Kuenga)
	// fmt.Printf("%+v", kuenga)
	// kuenga.print()
	// kuenga.updateName("Deki")
	// kuenga.print()
	kuengaPointer := &kuenga
	fmt.Printf("%p", kuengaPointer)
	kuenga.print()
	kuengaPointer.updateName("Deki")
	// kuenga.print()
	// kuenga.updateName("Pema")
	// kuenga.print()
}

type contactInfo struct {
	email  string
	mobile int
}

func (p person) print() {
	fmt.Printf("%+v", p)
}

func (pPointer *person) updateName(newfirstname string) {
	(*pPointer).firstname = newfirstname
}
